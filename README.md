# 微服务实例

本Demo涉及到的技术以及库主要包含：Golang、go-kit、gRPC、gin等。

Golang实现的微服务实例，包含网关、功能服务等。功能服务对外提供http，gRPC两种接口，网关对功能服务的调用以及系统内各个功能服务之间的调用同时支持http、gRPC。