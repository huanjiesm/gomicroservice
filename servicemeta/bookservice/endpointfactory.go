package bookservice

import (
	context "context"
	"io"
	"time"

	"encoding/json"
	"microservice/unit"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/ratelimit"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/tracing/zipkin"
	grpctransport "github.com/go-kit/kit/transport/grpc"
	stdzipkin "github.com/openzipkin/zipkin-go"
	"github.com/sony/gobreaker"
	"golang.org/x/time/rate"
	"google.golang.org/grpc"
)

// EndpointFactory 调用客户端
type EndpointFactory struct {
}

// Factory 创建请求工厂
func (f *EndpointFactory) Factory(zipkinTracer *stdzipkin.Tracer, logger log.Logger) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		// instance 是具体服务的ip:port  例如：127.0.0.1:8001
		conn, err := grpc.Dial(instance, grpc.WithInsecure())
		if err != nil {
			return nil, nil, err
		}
		ep, err := NewGrpcClient(conn, zipkinTracer, logger)
		if err != nil {
			return nil, nil, err
		}
		return ep, conn, nil
	}
}

// NewGrpcClient 新请求示例
func NewGrpcClient(conn *grpc.ClientConn, zipkinTracer *stdzipkin.Tracer, logger log.Logger) (endpoint.Endpoint, error) {
	limiter := ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Every(time.Second), 10000))
	zipkinClient := zipkin.GRPCClientTrace(zipkinTracer)
	options := []grpctransport.ClientOption{
		zipkinClient,
	}
	endp := grpctransport.NewClient(conn, "bookservice.BookService", "BookList",
		EncodeBookServiceRequest, DecodeBookServiceResponse,
		&BookInformationList{}, options...).Endpoint()

	endp = zipkin.TraceEndpoint(zipkinTracer, "BookService")(endp)
	endp = limiter(endp)
	endp = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
		Name:    "BookService",
		Timeout: time.Second * 100,
	}))(endp)

	return endp, nil
}

// EncodeBookServiceRequest 请求
func EncodeBookServiceRequest(_ context.Context, request interface{}) (interface{}, error) {
	return &Empty{}, nil
}

// DecodeBookServiceResponse 响应
func DecodeBookServiceResponse(_ context.Context, reply interface{}) (interface{}, error) {
	res := reply.(*BookInformationList)
	bodyBuf, _ := json.Marshal(res.BookInfoList)
	return &unit.ResponseComplexUnit{
		Body: bodyBuf,
	}, nil
}
