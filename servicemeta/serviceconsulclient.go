package servicemeta

import (
	"context"
	"io"
	"strings"
	"time"

	"microservice/unit"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/ratelimit"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/consul"
	consulsd "github.com/go-kit/kit/sd/consul"
	"github.com/go-kit/kit/sd/lb"
	"github.com/go-kit/kit/tracing/zipkin"
	grpctransport "github.com/go-kit/kit/transport/grpc"
	stdzipkin "github.com/openzipkin/zipkin-go"
	"github.com/sony/gobreaker"
	"golang.org/x/time/rate"
	"google.golang.org/grpc"
)

// ServiceConsulClient 中间件调用客户端
type ServiceConsulClient struct {
}

// NewServiceConsulClient 创建新对象
func NewServiceConsulClient() *ServiceConsulClient {
	return &ServiceConsulClient{}
}

// 每个服务还需要将自己注册到服务中心
// func (s* ServiceConsulClient) Register

// GeneratorEndpoint 创建...
func (s *ServiceConsulClient) GeneratorEndpoint(consulClient *consul.Client, tracer *stdzipkin.Tracer, logger log.Logger) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		r := request.(*unit.RequestComplexUnit)
		if strings.Contains(r.Headers.Get("X-Content-Type"), "application/grpc") {
			// 通过grpc的方式调用服务
			tags := []string{}
			passingOnly := true
			// 每一个APPNAME(也就是说每一个微服务)都需要一个单独的instancer,去consul获取服务需要【这里需要一个缓存】
			instancer := consulsd.NewInstancer(*consulClient, logger, r.AppName, tags, passingOnly)
			// // 构建endpoint
			// endFactory := bookservice.EndpointFactory{}
			// // endfactory实现了具体的转发请求。(目前仅支持http)
			// factory := endFactory.Factory(tracer, logger)
			// 根据consul.Instancer创建新的Endipoint
			endpointer := sd.NewEndpointer(instancer, func(instance string) (endpoint.Endpoint, io.Closer, error) {
				// instance 是具体服务的ip:port  例如：127.0.0.1:8001
				conn, err := grpc.Dial(instance, grpc.WithInsecure())
				if err != nil {
					return nil, nil, err
				}
				limiter := ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Every(time.Second), 10000))
				zipkinClient := zipkin.GRPCClientTrace(tracer)
				options := []grpctransport.ClientOption{
					zipkinClient,
				}
				x := make(map[interface{}]interface{})
				endp := grpctransport.NewClient(conn, "bookservice.BookService", r.Path,
					EncodeBookServiceRequest, DecodeBookServiceResponse,
					x, options...).Endpoint()

				endp = zipkin.TraceEndpoint(tracer, r.AppName)(endp)
				endp = limiter(endp)
				endp = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
					Name:    r.AppName,
					Timeout: time.Second * 100,
				}))(endp)
				if err != nil {
					return nil, nil, err
				}
				return endp, conn, nil
			}, logger)
			// 负载均衡
			balancer := lb.NewRoundRobin(endpointer)
			// 重试机制(次数，超时)
			retry := lb.Retry(3, 50*time.Second, balancer)

			// 实际应该认为存储的是consul.Instancer
			return retry(ctx, request)

		}

		// tags := []string{}
		// passingOnly := true
		// // 每一个APPNAME(也就是说每一个微服务)都需要一个单独的instancer【这里需要一个缓存】
		// instancer := consulsd.NewInstancer(*consulClient, logger, "grpc.health.v1.MultiService", tags, passingOnly)
		// // 构建endpoint
		// endFactory := bookservice.EndpointFactory{}
		// // endfactory实现了具体的转发请求。(目前仅支持http)
		// factory := endFactory.Factory(tracer, logger)
		// // 根据consul.Instancer创建新的Endipoint
		// endpointer := sd.NewEndpointer(instancer, factory, logger)
		// // 负载均衡
		// balancer := lb.NewRoundRobin(endpointer)
		// // 重试机制(次数，超时)
		// retry := lb.Retry(3, 50*time.Second, balancer)

		// // 实际应该认为存储的是consul.Instancer
		// return retry(ctx, request)
		return
	}
}

// GenerateGrpcEndpoint 创建适用于grpc通信的endpoint
func (s *ServiceConsulClient) GenerateGrpcEndpoint(consulClient *consul.Client, tracer *stdzipkin.Tracer, logger log.Logger) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		appName := ""
		methodName := ""
		// 通过grpc的方式调用服务
		tags := []string{}
		passingOnly := true
		// 每一个APPNAME(也就是说每一个微服务)都需要一个单独的instancer,去consul获取服务需要【这里需要一个缓存】
		instancer := consulsd.NewInstancer(*consulClient, logger, appName, tags, passingOnly)
		// // 构建endpoint
		// endFactory := bookservice.EndpointFactory{}
		// // endfactory实现了具体的转发请求。(目前仅支持http)
		// factory := endFactory.Factory(tracer, logger)
		// 根据consul.Instancer创建新的Endipoint
		endpointer := sd.NewEndpointer(instancer, func(instance string) (endpoint.Endpoint, io.Closer, error) {
			// instance 是具体服务的ip:port  例如：127.0.0.1:8001
			conn, err := grpc.Dial(instance, grpc.WithInsecure())
			if err != nil {
				return nil, nil, err
			}
			limiter := ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Every(time.Second), 10000))
			zipkinClient := zipkin.GRPCClientTrace(tracer)
			options := []grpctransport.ClientOption{
				zipkinClient,
			}
			x := make(map[interface{}]interface{})
			endp := grpctransport.NewClient(conn, "bookservice.BookService", methodName,
				EncodeBookServiceRequest, DecodeBookServiceResponse,
				x, options...).Endpoint()

			endp = zipkin.TraceEndpoint(tracer, appName)(endp)
			endp = limiter(endp)
			endp = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
				Name:    appName,
				Timeout: time.Second * 100,
			}))(endp)
			if err != nil {
				return nil, nil, err
			}
			return endp, conn, nil
		}, logger)
		// 负载均衡
		balancer := lb.NewRoundRobin(endpointer)
		// 重试机制(次数，超时)
		retry := lb.Retry(3, 50*time.Second, balancer)

		// 实际应该认为存储的是consul.Instancer
		return retry(ctx, request)

		// tags := []string{}
		// passingOnly := true
		// // 每一个APPNAME(也就是说每一个微服务)都需要一个单独的instancer【这里需要一个缓存】
		// instancer := consulsd.NewInstancer(*consulClient, logger, "grpc.health.v1.MultiService", tags, passingOnly)
		// // 构建endpoint
		// endFactory := bookservice.EndpointFactory{}
		// // endfactory实现了具体的转发请求。(目前仅支持http)
		// factory := endFactory.Factory(tracer, logger)
		// // 根据consul.Instancer创建新的Endipoint
		// endpointer := sd.NewEndpointer(instancer, factory, logger)
		// // 负载均衡
		// balancer := lb.NewRoundRobin(endpointer)
		// // 重试机制(次数，超时)
		// retry := lb.Retry(3, 50*time.Second, balancer)

		// // 实际应该认为存储的是consul.Instancer
		// return retry(ctx, request)
		// return
	}
}

// EncodeBookServiceRequest 请求
func EncodeBookServiceRequest(_ context.Context, request interface{}) (interface{}, error) {
	x := unit.UnitParams{
		SimpleMap: make(map[string]interface{}),
	}
	x.SimpleMap["t1"] = 4
	return &x, nil
}

// DecodeBookServiceResponse 响应
func DecodeBookServiceResponse(_ context.Context, reply interface{}) (interface{}, error) {
	// res := reply.(*BookInformationList)
	// bodyBuf, _ := json.Marshal(res.BookInfoList)
	return &unit.ResponseComplexUnit{
		Body: reply,
	}, nil
}
