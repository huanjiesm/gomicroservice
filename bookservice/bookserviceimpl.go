package main

import (
	"context"
	"microservice/servicemeta/bookservice"
)

// BookServiceImple  图书服务
type BookServiceImple struct{}

// BookList 书籍列表
func (b *BookServiceImple) BookList(ctx context.Context, req *bookservice.Empty) (*bookservice.BookInformationList, error) {
	return &bookservice.BookInformationList{
		BookInfoList: []*bookservice.BookInformation{
			&bookservice.BookInformation{
				ID:   2,
				Name: "高级编程",
			},
		},
	}, nil
}
