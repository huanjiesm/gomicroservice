package main

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	grpc "google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"

	"microservice/servicemeta/bookservice"
)

func main() {

	// 创建HTTP服务
	router := gin.Default()
	InstallRouter(router)
	// _ = h2c.NewHandler(router, &http2.Server{})

	// 创建gRPC服务
	grpcServer := grpc.NewServer()
	bookservice.RegisterBookServiceServer(grpcServer, &BookServiceImple{})

	// 注册服务
	consulReg := NewConsulRegister("consul.ms.com", "MultiService", "127.0.0.1", 8081, []string{"multiservice"})
	register, err := consulReg.NewConsulGRPCRegister()
	if err != nil {
		fmt.Println(err)
	}
	register.Register()
	// 健康检查
	// server := grpc.NewServer()
	grpc_health_v1.RegisterHealthServer(grpcServer, &HealthImpl{})

	// 启动服务
	server := &http.Server{
		Addr: ":8081",
		Handler: h2c.NewHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.ProtoMajor != 2 {
				router.ServeHTTP(w, r)
				return
			}
			if strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
				grpcServer.ServeHTTP(w, r)
				return
			}
			router.ServeHTTP(w, r)
		}), &http2.Server{}),
	}
	server.ListenAndServe()

}
