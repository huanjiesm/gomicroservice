package main

import (
	"context"

	"google.golang.org/grpc/health/grpc_health_v1"
)

// HealthImpl 健康检查实现
type HealthImpl struct{}

// Check 检查
func (h *HealthImpl) Check(ctx context.Context, req *grpc_health_v1.HealthCheckRequest) (*grpc_health_v1.HealthCheckResponse, error) {
	return &grpc_health_v1.HealthCheckResponse{
		Status: grpc_health_v1.HealthCheckResponse_SERVING,
	}, nil
}

// Watch 检查
func (h *HealthImpl) Watch(req *grpc_health_v1.HealthCheckRequest, watchserver grpc_health_v1.Health_WatchServer) error {
	return nil
}
