package main

import (
	"fmt"
	"os"
	"time"

	"github.com/go-kit/kit/log"
	consulsd "github.com/go-kit/kit/sd/consul"
	"github.com/hashicorp/consul/api"
)

// ConsulRegister 服务发现注册
type ConsulRegister struct {
	ConsulAddress                  string
	ServiceName                    string
	ServiceIP                      string
	Tags                           []string
	ServicePort                    int
	DeregisterCriticalServiceAfter time.Duration
	Interval                       time.Duration
}

// NewConsulRegister 创建新的注册对象
func NewConsulRegister(consulAddr string, serviceName string, serviceIP string, servicePort int, tags []string) *ConsulRegister {
	return &ConsulRegister{
		ConsulAddress:                  consulAddr,
		ServiceName:                    serviceName,
		ServiceIP:                      serviceIP,
		ServicePort:                    servicePort,
		Tags:                           tags,
		DeregisterCriticalServiceAfter: time.Minute,
		Interval:                       10 * time.Second,
	}
}

// NewConsulGRPCRegister 创建grpc注册器
func (r *ConsulRegister) NewConsulGRPCRegister() (*consulsd.Registrar, error) {
	var logger log.Logger
	logger = log.NewLogfmtLogger(os.Stderr)
	logger = log.With(logger, "ts", log.DefaultTimestamp)
	logger = log.With(logger, "caller", log.DefaultCaller)

	consulConfig := api.DefaultConfig()
	consulConfig.Address = r.ConsulAddress
	consulClient, err := api.NewClient(consulConfig)
	if err != nil {
		return nil, err
	}
	client := consulsd.NewClient(consulClient)

	reg := &api.AgentServiceRegistration{
		ID:      fmt.Sprintf("%v-%v-%v", r.ServiceName, r.ServiceIP, r.ServicePort),
		Name:    fmt.Sprintf("grpc.health.v1.%v", r.ServiceName),
		Tags:    r.Tags,
		Port:    r.ServicePort,
		Address: r.ServiceIP,
		Check: &api.AgentServiceCheck{
			Interval:                       r.Interval.String(),
			GRPC:                           fmt.Sprintf("%v:%v/%v", r.ServiceIP, r.ServicePort, r.ServiceName),
			DeregisterCriticalServiceAfter: r.DeregisterCriticalServiceAfter.String(),
		},
	}

	return consulsd.NewRegistrar(client, reg, logger), nil

}
