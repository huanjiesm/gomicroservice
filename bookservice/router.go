package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// InstallRouter 注册路由
func InstallRouter(router *gin.Engine) {

	router.GET("/bookinfo/:id", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"id":   c.Param("id"),
			"name": "编程",
		})
	})

	router.GET("/booklist", func(c *gin.Context) {
		c.JSON(http.StatusOK, []gin.H{
			gin.H{
				"id":   c.Param("id"),
				"name": "编程",
			},
		})
	})

	router.GET("/addbook", func(c *gin.Context) {
		c.JSON(http.StatusOK, 3)
	})

}
