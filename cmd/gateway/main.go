package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"microservice/gateway"

	"golang.org/x/net/http2/h2c"
	"golang.org/x/net/http2"
)

func main() {

	// 解析参数
	httpAddr := flag.String("port", ":8000", "Address Port for HTTP server")
	consulAddr := flag.String("consuladdr", "http://consul.ms.com", "Consul agent address")
	zipkinURL := flag.String("consulurl", "http://zipkin.ms.com/api/v2/spans", "zipkin server address")
	retryMax := flag.Int("retrymax", 3, "per-request retries to different instances")
	retryTimeout := flag.Int("retrytimeout", 500, "per-request timeout, including retries")
	flag.Parse()

	// 初始化基础服务对象
	err := gateway.AssistInstance().Initialize(*consulAddr, *zipkinURL, *retryMax, *retryTimeout)
	if err != nil {
		fmt.Println("初始化系统服务对象失败，系统将退出")
		os.Exit(2)
		return
	}
	// 创建代理路由
	router := gateway.NewProxyRouter()
	// 启动服务
	errc := make(chan error) // 通道默认是阻塞的。
	go func() {
		// 收到系统信号：程序终止信号【Ctrl+C】, 程序退出【kill命令】
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errc <- fmt.Errorf("%s", <-c) // 通知系统退出
	}()

	// // HTTP transport.
	go func() {
		gateway.AssistInstance().Logger().Log("transport", "HTTP", "addr", *httpAddr)
		errc <- http.ListenAndServe(*httpAddr, h2c.NewHandler(router,&http2.Server{})) // 服务异常，系统退出
	}()
	// Run!
	// 系统的主线程会在此阻塞。只有收到终止信号或者是http监听退出才会继续执行并退出系统。
	gateway.AssistInstance().Logger().Log("exit:", <-errc)

}
