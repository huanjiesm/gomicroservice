package gateway

import (
	"context"
	"net/http"
	"strings"

	"microservice/servicemeta"
	"microservice/unit"

	grpctransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

// NewProxyRouter 创建代理路由
func NewProxyRouter() http.Handler {
	// 路由
	// httpServe := httptransport.NewServer(
	// 	servicemeta.NewServiceConsulClient().GeneratorEndpoint(AssistInstance().ConsulClient(), AssistInstance().ZipkinTracer(), AssistInstance().Logger()),
	// 	decodeRequest,
	// 	encodeResponse,
	// )

	grpcServe := grpctransport.NewServer(servicemeta.NewServiceConsulClient().GenerateGrpcEndpoint(AssistInstance().ConsulClient(), AssistInstance().ZipkinTracer(), AssistInstance().Logger()),
		decodeGrpcRequest, encodeGrpcResponse)

	// router := mux.NewRouter()
	// // router.HandleFunc("/{appname}/{subpath:.*}", func(w http.ResponseWriter, r *http.Request) {
	// // 	if r.ProtoMajor != 2 {
	// // 		httpServe.ServeHTTP(w, r)
	// // 		return
	// // 	}
	// // 	if strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
	// // 		grpcServe.ServeHTTP(w, r)
	// // 		return
	// // 	}
	// // 	httpServe.ServeHTTP(w, r)
	// // })
	// router.Handle("/{appname}/{subpath:.*}", httpServe)

	return grpcServe
}

// decodeRequest 代理请求
func decodeRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	reqUnit := &unit.RequestComplexUnit{
		Headers: r.Header,
		Method:  r.Method,
		Path:    vars["subpath"],
		AppName: vars["appname"],
		Body:    r.Body,
		Query:   r.URL.Query(),
	}
	return reqUnit, nil
}

// encodeResponse 代理响应
func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	resp := response.(*unit.ResponseComplexUnit)
	// 写入Header
	for key, val := range resp.Headers {
		w.Header().Set(key, strings.Join(val, ","))
	}
	// 配置全局跨域
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "GET,POST,OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "*,Content-Type")

	// body := resp.Body.(io.ReadCloser)
	// defer body.Close()

	// bodyBuf, err := ioutil.ReadAll(body)
	// if err != nil {
	// 	fmt.Println("代理请求失败：", err.Error())
	// }
	// 写入响应数据
	w.Write(resp.Body.([]byte))
	// _, err := io.Copy(w, body)
	// if err != nil {
	// 	fmt.Println("代理请求失败：", err.Error())
	// }
	return nil
}

// encodeGrpcRequest
func decodeGrpcRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return nil, nil
}

func encodeGrpcResponse(_ context.Context, w http.ResponseWriter, reply interface{}) error {
	return nil
}
