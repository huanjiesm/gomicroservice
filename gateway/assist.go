package gateway

import (
	"os"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd/consul"
	"github.com/hashicorp/consul/api"
	"github.com/openzipkin/zipkin-go"
	zipkinhttp "github.com/openzipkin/zipkin-go/reporter/http"
)

// Assist 全局对象以及相关配置
type Assist struct {
	logger         log.Logger
	consulsdClient *consul.Client
	zipkinTracer   *zipkin.Tracer

	retryMax     int
	retryTimeout time.Duration
}

var assistIns *Assist
var assistOnce sync.Once

// AssistInstance 单例
func AssistInstance() *Assist {
	assistOnce.Do(func() {
		assistIns = &Assist{}
	})
	return assistIns
}

// Initialize 根据配置项初始化相关对象
func (a *Assist) Initialize(consulAddr string, zipkinAddr string, retry int, retryTimeout int) error {
	// 日志
	logger := log.NewLogfmtLogger(os.Stderr)
	logger = log.With(logger, "TimeStamp", log.DefaultTimestampUTC)
	a.logger = log.With(logger, "Caller", log.DefaultCaller)

	// 服务发现
	consulConfig := api.DefaultConfig()
	if len(consulAddr) > 0 {
		consulConfig.Address = consulAddr
	}
	client, err := api.NewClient(consulConfig)
	if err != nil {
		return err
	}
	consulClient := consul.NewClient(client)
	a.consulsdClient = &consulClient
	// 链路追踪？
	a.zipkinTracer, _ = zipkin.NewTracer(zipkinhttp.NewReporter(zipkinAddr), zipkin.WithTraceID128Bit(true))

	// 重试时间间隔
	a.retryTimeout = time.Duration(retryTimeout) * time.Millisecond

	return nil
}

// Logger 获取日志对象
func (a *Assist) Logger() log.Logger {
	return a.logger
}

// ConsulClient Consul对象
func (a *Assist) ConsulClient() *consul.Client {
	return a.consulsdClient
}

// ZipkinTracer 链路追踪器
func (a *Assist) ZipkinTracer() *zipkin.Tracer {
	return a.zipkinTracer
}

// RetryMax 重试次数
func (a *Assist) RetryMax() int {
	return a.retryMax
}

// RetryTimeout 重试超时时间
func (a *Assist) RetryTimeout() time.Duration {
	return a.retryTimeout
}
