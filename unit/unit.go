/*
* 代理服务中使用的组合参数
* unit params for proxy service
* Copyright(c) 2019 liumurong
**/

package unit

import (
	"net/http"
	"net/url"

	proto "github.com/golang/protobuf/proto"
)

// RequestComplexUnit 请求参数
type RequestComplexUnit struct {
	Headers http.Header // 头
	Method  string
	Path    string      // 路径，不包含域名等内容
	AppName string      // 微服务名称
	Body    interface{} // 内容
	Query   url.Values  // URL查询部分内容
}

// ResponseComplexUnit 响应参数
type ResponseComplexUnit struct {
	Headers http.Header
	Body    interface{}
}

// SimpleMap xx
type SimpleMap map[string]interface{}

// UnitParams xx
type UnitParams struct {
	SimpleMap
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

// Reset xx
func (m *UnitParams) Reset() {
	*m = UnitParams{
		SimpleMap: make(map[string]interface{}),
	}
}
func (m *UnitParams) String() string { return proto.CompactTextString(m) }

// ProtoMessage xx
func (*UnitParams) ProtoMessage() {}

// Descriptor xx
func (*UnitParams) Descriptor() ([]byte, []int) {
	return []byte{}, []int{1}
}

// XXX_Unmarshal xx
func (m *UnitParams) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Params.Unmarshal(m, b)
}

// XXX_Marshal xx
func (m *UnitParams) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Params.Marshal(b, m, deterministic)
}

// XXX_Merge xx
func (m *UnitParams) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Params.Merge(m, src)
}

// XXX_Size xx
func (m *UnitParams) XXX_Size() int {
	return xxx_messageInfo_Params.Size(m)
}

// XXX_DiscardUnknown xx
func (m *UnitParams) XXX_DiscardUnknown() {
	xxx_messageInfo_Params.DiscardUnknown(m)
}

var xxx_messageInfo_Params proto.InternalMessageInfo
